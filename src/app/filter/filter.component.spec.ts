import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { FilterComponent } from './filter.component';

import { FilterService } from './filter.service';
import { Observable } from 'rxjs/Observable';


export class MockFilterService extends FilterService {
  getReginalbloc = () => {
    return new Observable(observer => {
    observer.next([{'name': 'TOTOTOTO', 'code': 'AF'},
                  {'name': 'TITITIT', 'code': 'AX'}]);
    observer.complete();
  });
  }
}

describe('FilterComponent', () => {
  let component: FilterComponent;
  let fixture: ComponentFixture<FilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports : [
        HttpClientModule,
        ReactiveFormsModule
      ],
      providers : [
        { provide: FilterService, useClass: MockFilterService }
      ],
      declarations: [ FilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form is not valid', () => {
    expect(component.filterForm.valid).toBeFalsy();
  });

  it('from : test code AZERTY', () => {
    component.filterForm.controls['code'].setValue('AZERTY');
    expect(component.filterForm.controls['code'].valid).toBeFalsy();
  });

  it('from : test code 9', () => {
    component.filterForm.controls['code'].setValue(9);
    expect(component.filterForm.controls['code'].valid).toBeTruthy();
  });
});

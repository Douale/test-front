import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FilterService } from './filter.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  public filterForm: FormGroup;
  public countries: any[];

  constructor(
    private fb: FormBuilder,
    private filterService: FilterService
  ) { }

  ngOnInit() {
    this.getCountries();
    this.initFilterForm();
  }

  private initFilterForm() {
    this.filterForm = this.fb.group({
      description : [
        undefined,
        Validators.compose([Validators.required])
      ],
      code : [
        'AZE',
        Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(8), Validators.pattern('[0-9]')])
      ],
      country : [
        undefined,
        Validators.compose([Validators.required])
      ]
    });
  }

  public saveFilterForm() {
    if ( !this.filterForm.valid) {
      return;
    }
  }

  private getCountries() {
    this.filterService.getReginalbloc().subscribe(countries => {
      this.countries = countries;
    });
  }

}
